# LeCompteEstBon

How to use

Jeu du compte est bon

Dès le lancement du jeu, il faut choisir entre le mode de jeu ou le solveur

Chacun de ces deux modes comporte une fonction de remplissage des nombres manuelle
ou automatique (nombres générés aléatoirement selon les critères définis par les règles du jeu)

Le solveur est entièrement fonctionnel. Le jeu quant à lui comporte encore des erreurs lors de l'entrée des calculs par le joueur